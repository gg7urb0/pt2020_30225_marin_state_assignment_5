

import java.io.IOException;
import java.text.ParseException;


public class MainClass {
	
	
	
	public static void main(String[] args) throws IOException, ParseException {
		Methods m = new Methods();
		m.task1();
		m.task2();
		m.task3();
		m.task4();
		m.task5();
		m.task6();
	}
	
}
	