import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;


public class Methods{

	public Date start = new Date();
	public Date end = new Date();
	public String activitati = new String();
	public List<Methods> date;
	
	public void parse() throws IOException, ParseException{
		date = new ArrayList<>();
		Stream<String> stream = Stream.empty();
		stream = Files.lines(Paths.get("Activities.txt"));
		List<String> monitoredString = stream.map(s -> s.split("\\s{2,100}")).flatMap(Arrays::stream).collect(Collectors.toList());
		for(int i = 0; i < monitoredString.size() - 2; i+=3)
			date.add(new Methods(new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss").parse(monitoredString.get(i)), new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss").parse(monitoredString.get(i+1)), monitoredString.get(i+2)));
		
	}
	
	public void task1() throws IOException, ParseException {
		PrintWriter writer = new PrintWriter("task_1.txt", "UTF-8");
		Methods m = new Methods();
		m.date.stream().forEach(e->writer.println(e.start+" "+e.end+" "+e.getActivity()));
		writer.close();
		
	}
	
	public void task2(){
		try{
		    PrintWriter writer = new PrintWriter("task_2.txt", "UTF-8");
				writer.println("the number of distinct days is: "+ date.stream().map(s -> s.getDate()).distinct().count());
		    writer.close();
		} catch (IOException e) {
		}
	}
	
	public void task3(){
		Map<String, Long> actions = date.stream().collect(Collectors.groupingBy(Methods::getActivity, Collectors.counting()));
		
		try{
		    PrintWriter writer = new PrintWriter("task_3.txt", "UTF-8");
		    for( HashMap.Entry<String, Long> entry : actions.entrySet() ){
				writer.println(entry.getKey() + " occurs " + entry.getValue() + " times." );
			}
		    writer.close();
		} catch (IOException e) {
		}
	}
	
	public void task4(){
		Map<Integer, Map<String, Long>> eachDay = date.stream().collect(Collectors.groupingBy(Methods::getDate, Collectors.groupingBy(Methods::getActivity, Collectors.counting())));
		
		try{
		    PrintWriter writer = new PrintWriter("task_4.txt", "UTF-8");
		    for( HashMap.Entry<Integer, Map<String, Long>> entry : eachDay.entrySet() ){
				writer.println("Day " + entry.getKey()+ " ");
				for(HashMap.Entry<String, Long> e: entry.getValue().entrySet())
					writer.println(e.getKey() + " " + e.getValue());
				writer.println('\n');
			}
		    writer.close();
		} catch (IOException e) {
		}
		
	}
	
	public void task5(){
	
		Map<String, Duration> period = date.stream().collect(Collectors.toMap(var -> var.activitati,
					       var  -> new Duration(new DateTime(var.start), new DateTime(var.end)), Duration::plus));
		Map<String, Duration> period2 = period.entrySet().stream().filter(map -> map.getValue().getStandardHours() > 0).collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
		try{
		    PrintWriter writer = new PrintWriter("task_5.txt", "UTF-8");
		    for( HashMap.Entry<String, Duration> entry : period2.entrySet() ){
				writer.println(entry.getKey() + "	-- " + entry.getValue().getStandardMinutes()+ " minutes");	
			}
		    writer.close();
		} catch (IOException e) {
		}
	}
	
	public void task6(){
		Map<String, Long> actions = date.stream().collect(Collectors.groupingBy(Methods::getActivity, Collectors.counting()));
		Map<String, Long> actions2 = date.stream().filter((var -> new Duration(new DateTime(var.start), new DateTime(var.end)).getStandardMinutes() < 5)).collect(Collectors.groupingBy(Methods::getActivity, Collectors.counting()));
		List<String> list = new ArrayList<String>();
		
		actions.forEach((key1,v1) -> { actions2.forEach((key2, v2) -> {
			if(key1.equals(key2))
				if(v2 >= (v1 * 9) / 10)
					list.add(key1);
		});});
				
		try{
		 PrintWriter writer = new PrintWriter("task_6.txt", "UTF-8");
		 for(String s: list)
				writer.println(s);
		 writer.close();
		} catch (IOException e) {
		}
		
	}
	
	Methods(Date _startTime, Date _endTime, String _activityLabel) {
		this.start = _startTime;
		this.end = _endTime;
		this.activitati = _activityLabel;
		
	}
	
	Methods() throws IOException, ParseException{
		parse();
	}
	
	public int getDate() {
		return start.getDate();
	}
	
	public DateTime getDateTime(){
		Duration d = new Duration(start);
		DateTime endt = new DateTime(end);
	
		return endt.minus(d);
	}	

	public String getActivity() {
		return activitati;
	}

}